/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "MantleAPI/Common/log_utils.h"

#include <gtest/gtest.h>

#include <limits>
#include <sstream>
#include <type_traits>

#include "MantleAPI/Common/i_logger.h"

namespace
{

using mantle_api::LogLevel;
using mantle_api::log_utils::ToStringView;

static_assert(ToStringView(LogLevel::kTrace) == "Trace");
static_assert(ToStringView(LogLevel::kDebug) == "Debug");
static_assert(ToStringView(LogLevel::kInfo) == "Info");
static_assert(ToStringView(LogLevel::kWarning) == "Warning");
static_assert(ToStringView(LogLevel::kError) == "Error");
static_assert(ToStringView(LogLevel::kCritical) == "Critical");

TEST(LogUtilsTest, ToStringView)
{
  ASSERT_STREQ(ToStringView(LogLevel::kTrace).data(), "Trace");
  ASSERT_STREQ(ToStringView(LogLevel::kDebug).data(), "Debug");
  ASSERT_STREQ(ToStringView(LogLevel::kInfo).data(), "Info");
  ASSERT_STREQ(ToStringView(LogLevel::kWarning).data(), "Warning");
  ASSERT_STREQ(ToStringView(LogLevel::kError).data(), "Error");
  ASSERT_STREQ(ToStringView(LogLevel::kCritical).data(), "Critical");

  ASSERT_STREQ(ToStringView(static_cast<LogLevel>(std::numeric_limits<std::underlying_type_t<LogLevel>>::min())).data(), "Log level out of range");
  ASSERT_STREQ(ToStringView(static_cast<LogLevel>(std::numeric_limits<std::underlying_type_t<LogLevel>>::max())).data(), "Log level out of range");
}

TEST(LogUtilsTest, OutputStreamOperator)
{
  {
    auto stream = std::ostringstream{};
    ASSERT_NO_THROW(stream << LogLevel::kTrace);
    ASSERT_STREQ(stream.str().c_str(), "Trace");
  }
  {
    auto stream = std::ostringstream{};
    ASSERT_NO_THROW(stream << LogLevel::kDebug);
    ASSERT_STREQ(stream.str().c_str(), "Debug");
  }
  {
    auto stream = std::ostringstream{};
    ASSERT_NO_THROW(stream << LogLevel::kInfo);
    ASSERT_STREQ(stream.str().c_str(), "Info");
  }
  {
    auto stream = std::ostringstream{};
    ASSERT_NO_THROW(stream << LogLevel::kWarning);
    ASSERT_STREQ(stream.str().c_str(), "Warning");
  }
  {
    auto stream = std::ostringstream{};
    ASSERT_NO_THROW(stream << LogLevel::kError);
    ASSERT_STREQ(stream.str().c_str(), "Error");
  }
  {
    auto stream = std::ostringstream{};
    ASSERT_NO_THROW(stream << LogLevel::kCritical);
    ASSERT_STREQ(stream.str().c_str(), "Critical");
  }
}

}  // namespace
