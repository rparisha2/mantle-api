/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "MantleAPI/Common/floating_point_helper.h"

#include <gtest/gtest.h>
#include <units.h>

#include <limits>

namespace
{

using mantle_api::AlmostEqual;
using mantle_api::GreaterOrEqual;
using mantle_api::LessOrEqual;
using units::length::meter_t;

//
// double
//
inline constexpr auto kDoubleZero = 0.0;
inline constexpr auto kDoubleEpsilon = std::numeric_limits<double>::epsilon();
inline constexpr auto kDoubleLowest = std::numeric_limits<double>::lowest();
inline constexpr auto kDoubleMax = std::numeric_limits<double>::max();
inline constexpr auto kDoubleInfinity = std::numeric_limits<double>::infinity();
inline constexpr auto kDoubleNaN = std::numeric_limits<double>::quiet_NaN();

// AlmostEqual
static_assert(AlmostEqual(kDoubleZero, kDoubleZero));
static_assert(AlmostEqual(kDoubleEpsilon, kDoubleEpsilon));
static_assert(AlmostEqual(kDoubleLowest, kDoubleLowest));
static_assert(AlmostEqual(kDoubleMax, kDoubleMax));
static_assert(AlmostEqual(kDoubleEpsilon, kDoubleZero));
static_assert(AlmostEqual(kDoubleMax - kDoubleEpsilon, kDoubleMax));
static_assert(AlmostEqual(kDoubleLowest + kDoubleEpsilon, kDoubleLowest));

static_assert(AlmostEqual(kDoubleInfinity, kDoubleInfinity));
static_assert(AlmostEqual(-kDoubleInfinity, -kDoubleInfinity));
static_assert(!AlmostEqual(kDoubleInfinity, -kDoubleInfinity));
static_assert(!AlmostEqual(kDoubleLowest, -kDoubleInfinity));
static_assert(!AlmostEqual(kDoubleMax, kDoubleInfinity));

static_assert(!AlmostEqual(kDoubleNaN, kDoubleZero));
static_assert(!AlmostEqual(kDoubleNaN, kDoubleEpsilon));
static_assert(!AlmostEqual(kDoubleNaN, kDoubleLowest));
static_assert(!AlmostEqual(kDoubleNaN, kDoubleMax));
static_assert(!AlmostEqual(kDoubleNaN, kDoubleNaN));

// GreaterOrEqual
static_assert(GreaterOrEqual(kDoubleZero, kDoubleZero));
static_assert(GreaterOrEqual(kDoubleEpsilon, kDoubleEpsilon));
static_assert(GreaterOrEqual(kDoubleLowest, kDoubleLowest));
static_assert(GreaterOrEqual(kDoubleMax, kDoubleMax));
static_assert(GreaterOrEqual(kDoubleEpsilon, kDoubleZero));
static_assert(GreaterOrEqual(kDoubleInfinity, kDoubleMax));

// LessOrEqual
static_assert(LessOrEqual(kDoubleZero, kDoubleZero));
static_assert(LessOrEqual(kDoubleEpsilon, kDoubleEpsilon));
static_assert(LessOrEqual(kDoubleLowest, kDoubleLowest));
static_assert(LessOrEqual(kDoubleMax, kDoubleMax));
static_assert(LessOrEqual(kDoubleZero, kDoubleEpsilon));
static_assert(LessOrEqual(-kDoubleInfinity, kDoubleLowest));

//
// float
//
inline constexpr auto kFloatZero = 0.0F;
inline constexpr auto kFloatEpsilon = std::numeric_limits<float>::epsilon();
inline constexpr auto kFloatLowest = std::numeric_limits<float>::lowest();
inline constexpr auto kFloatMax = std::numeric_limits<float>::max();
inline constexpr auto kFloatInfinity = std::numeric_limits<float>::infinity();
inline constexpr auto kFloatNaN = std::numeric_limits<float>::quiet_NaN();

// AlmostEqual
static_assert(AlmostEqual(kFloatZero, kFloatZero));
static_assert(AlmostEqual(kFloatEpsilon, kFloatEpsilon));
static_assert(AlmostEqual(kFloatLowest, kFloatLowest));
static_assert(AlmostEqual(kFloatMax, kFloatMax));
static_assert(AlmostEqual(kFloatEpsilon, kFloatZero));
static_assert(AlmostEqual(kFloatMax - kFloatEpsilon, kFloatMax));
static_assert(AlmostEqual(kFloatLowest + kFloatEpsilon, kFloatLowest));

static_assert(AlmostEqual(kFloatInfinity, kFloatInfinity));
static_assert(AlmostEqual(-kFloatInfinity, -kFloatInfinity));
static_assert(!AlmostEqual(kFloatInfinity, -kFloatInfinity));
static_assert(!AlmostEqual(kFloatLowest, -kFloatInfinity));
static_assert(!AlmostEqual(kFloatMax, kFloatInfinity));

static_assert(!AlmostEqual(kFloatNaN, kFloatZero));
static_assert(!AlmostEqual(kFloatNaN, kFloatEpsilon));
static_assert(!AlmostEqual(kFloatNaN, kFloatLowest));
static_assert(!AlmostEqual(kFloatNaN, kFloatMax));
static_assert(!AlmostEqual(kFloatNaN, kFloatNaN));

// GreaterOrEqual
static_assert(GreaterOrEqual(kFloatZero, kFloatZero));
static_assert(GreaterOrEqual(kFloatEpsilon, kFloatEpsilon));
static_assert(GreaterOrEqual(kFloatLowest, kFloatLowest));
static_assert(GreaterOrEqual(kFloatMax, kFloatMax));
static_assert(GreaterOrEqual(kFloatEpsilon, kFloatZero));
static_assert(GreaterOrEqual(kFloatInfinity, kFloatMax));

// LessOrEqual
static_assert(LessOrEqual(kFloatZero, kFloatZero));
static_assert(LessOrEqual(kFloatEpsilon, kFloatEpsilon));
static_assert(LessOrEqual(kFloatLowest, kFloatLowest));
static_assert(LessOrEqual(kFloatMax, kFloatMax));
static_assert(LessOrEqual(kFloatZero, kFloatEpsilon));
static_assert(LessOrEqual(-kFloatInfinity, kFloatLowest));

//
// meter_t
//
inline constexpr auto kMeterZero = meter_t{kDoubleZero};
inline constexpr auto kMeterEpsilon = meter_t{kDoubleEpsilon};
inline constexpr auto kMeterLowest = meter_t{kDoubleLowest};
inline constexpr auto kMeterMax = meter_t{kDoubleMax};
inline constexpr auto kMeterInfinity = meter_t{kDoubleInfinity};
inline constexpr auto kMeterNaN = meter_t{kDoubleNaN};

// AlmostEqual
static_assert(AlmostEqual(kMeterZero, kMeterZero));
static_assert(AlmostEqual(kMeterEpsilon, kMeterEpsilon));
static_assert(AlmostEqual(kMeterLowest, kMeterLowest));
static_assert(AlmostEqual(kMeterMax, kMeterMax));
static_assert(AlmostEqual(kMeterEpsilon, kMeterZero));
static_assert(AlmostEqual(kMeterMax - kMeterEpsilon, kMeterMax));
static_assert(AlmostEqual(kMeterLowest + kMeterEpsilon, kMeterLowest));

static_assert(AlmostEqual(kMeterInfinity, kMeterInfinity));
static_assert(AlmostEqual(-kMeterInfinity, -kMeterInfinity));
static_assert(!AlmostEqual(kMeterInfinity, -kMeterInfinity));
static_assert(!AlmostEqual(kMeterLowest, -kMeterInfinity));
static_assert(!AlmostEqual(kMeterMax, kMeterInfinity));

static_assert(!AlmostEqual(kMeterNaN, kMeterZero));
static_assert(!AlmostEqual(kMeterNaN, kMeterEpsilon));
static_assert(!AlmostEqual(kMeterNaN, kMeterLowest));
static_assert(!AlmostEqual(kMeterNaN, kMeterMax));
static_assert(!AlmostEqual(kMeterNaN, kMeterNaN));

// GreaterOrEqual
static_assert(GreaterOrEqual(kMeterZero, kMeterZero));
static_assert(GreaterOrEqual(kMeterEpsilon, kMeterEpsilon));
static_assert(GreaterOrEqual(kMeterLowest, kMeterLowest));
static_assert(GreaterOrEqual(kMeterMax, kMeterMax));
static_assert(GreaterOrEqual(kMeterEpsilon, kMeterZero));
static_assert(GreaterOrEqual(kMeterInfinity, kMeterMax));

// LessOrEqual
static_assert(LessOrEqual(kMeterZero, kMeterZero));
static_assert(LessOrEqual(kMeterEpsilon, kMeterEpsilon));
static_assert(LessOrEqual(kMeterLowest, kMeterLowest));
static_assert(LessOrEqual(kMeterMax, kMeterMax));
static_assert(LessOrEqual(kMeterZero, kMeterEpsilon));
static_assert(LessOrEqual(-kMeterInfinity, kMeterLowest));

}  // namespace
